from setuptools import setup, find_packages

setup(
    name='model-zuora',
    version='0.1',
    description='Meltano .m5o models for data fetched using the Zuora API',
    packages=find_packages(),
    package_data={'models': ['*.m5o']},
    install_requires=[],
)
